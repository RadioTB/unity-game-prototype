﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class FairyLightController : MonoBehaviour {

    public void Update()
    {
        
    }

    public void ChangeColor()
    {
        Light light = GetComponent<Light>();
        light.color = Color.cyan;
    }
}
