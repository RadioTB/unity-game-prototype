﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Plane))]
public class Floor : MonoBehaviour {


    public Plane GetPlane()
    {
        Plane plane = GetComponent<Plane>();
        return plane;
    }
}
