﻿using UnityEngine;
using System.Collections;

public class MoveCloseLight : MonoBehaviour {
    GameObject player;
    Light player_light;
    GameObject fairy;
    Vector3 player_position;
    float p_light_distance;
    Ray player_to_fairy;
    float P_LIGHT_Y;
    int wall_mask;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Thief");
        fairy = GameObject.FindGameObjectWithTag("Fairy");
        player_light = GameObject.FindGameObjectWithTag("Thief_Light").GetComponent<Light>();

        p_light_distance = Vector3.Distance(player.transform.position, player_light.transform.position);


        P_LIGHT_Y = player_light.transform.position.y;


        player_to_fairy = new Ray(fairy.transform.position, player.transform.position - fairy.transform.position);
        wall_mask = 1 << 8;

    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;

        player_to_fairy = new Ray(player.transform.position, fairy.transform.position - player.transform.position);
        Debug.DrawRay(player.transform.position, fairy.transform.position - player.transform.position, Color.red, 1);

        if (!Physics.Raycast(player_to_fairy, out hit, Vector3.Distance(fairy.transform.position, player.transform.position), wall_mask))
        {
            player_light.enabled = true;
        }
        else
        {
            player_light.enabled = false;
        }

        player_light.transform.position = player_to_fairy.GetPoint(p_light_distance);

    }
}
