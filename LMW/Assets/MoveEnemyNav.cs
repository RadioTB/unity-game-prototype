﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class MoveEnemyNav : MonoBehaviour {
    UnityEngine.AI.NavMeshAgent enemy_nma;
    GameObject fairy_go;
	// Use this for initialization
	void Awake () {
        fairy_go = GameObject.FindGameObjectWithTag("Thief");
        enemy_nma = GetComponent<UnityEngine.AI.NavMeshAgent>();
        enemy_nma.updateRotation = false;
        enemy_nma.speed = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
        enemy_nma.SetDestination(fairy_go.transform.position);
	}
}
