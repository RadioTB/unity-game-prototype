﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]

public class MoveFairy : MonoBehaviour {


    float SPEED = 3f;
    Rigidbody fairy_rb;
    Vector3 mouse_position;
    Vector3 mouse_position_holder;
    Vector3 direction;

    bool on_wall;
    bool sliding;


    Vector3 GetVelocity(Vector3 v1, Vector3 v2)
    {
        return ((v2 - v1) / Vector3.Distance(v1, v2)).normalized * SPEED;
    }


    Vector3 PerpendicularCounter(Vector3 v1)
    {
        return new Vector3(-v1.z, v1.y, v1.x).normalized;
    }


    Vector3 PerpendicularClock(Vector3 v1)
    {
        return new Vector3(v1.z, v1.y, -v1.x).normalized;
    }


    void Slide(Vector3 rd, Vector3 click_position)
    {
        float clockwise = Vector3.Cross(rd.normalized, click_position.normalized).y;

        Debug.DrawRay(transform.position, rd, Color.green, 2);
        Debug.DrawRay(transform.position, click_position, Color.green, 2);

        Vector3 dir = Vector3.zero;

        if (clockwise < 0)
        {
            dir = PerpendicularCounter(rd);
        }
        else if (clockwise >= 0)
        {
            dir = PerpendicularClock(rd);
        }

        float angle = Vector3.Angle(dir, click_position) * Mathf.PI / 180;

        float mag = Mathf.Cos(angle) * click_position.magnitude;

        Debug.Log("dir: " + dir + " mag: " + mag);

        mouse_position = transform.position + (dir * mag);

        Debug.DrawRay(transform.position, dir * mag, Color.green, 2);

        Debug.DrawRay(Vector3.zero, mouse_position, Color.cyan, 3);


        Debug.Log("FINAL DIRECTION " + mouse_position);
        fairy_rb.velocity = (dir.normalized * SPEED);
        sliding = false;
    }





    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Wall")
        {
            mouse_position_holder = mouse_position;
            Debug.Log("Entered Collision");
            fairy_rb.velocity = (Vector3.zero);

            /*if (on_wall)
            {
                on_corner = true;
                Debug.Log(on_corner);
            }
            else
            {*/
                on_wall = true;
                sliding = true;
            //}
        }
        
    
    }
    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Wall")
        {
            /*(if (on_corner)
            {
                fairy_rb.velocity = GetVelocity(new Vector3(col.contacts[0].point.x, transform.position.y, col.contacts[0].point.z), transform.position);
                return;
            }*/

            if (Input.GetMouseButtonDown(0) && on_wall)
            {
                mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mouse_position = new Vector3(mouse_position.x, transform.position.y, mouse_position.z);
                mouse_position_holder = mouse_position;

                Debug.DrawRay(transform.position, mouse_position - transform.position, Color.yellow, 1);

                Debug.Log("CONTACT POINTS " + col.contacts.Length);
                Debug.Log("CONTACT POINT " + col.contacts[0].point);

                Vector3 to_click = mouse_position - transform.position;
                Vector3 to_col = new Vector3(col.contacts[0].point.x, transform.position.y, col.contacts[0].point.z) - transform.position;


                float angle = Vector3.Angle(to_col, to_click);



                if (angle > 89)
                {
                    fairy_rb.velocity = GetVelocity(transform.position, mouse_position);
                    sliding = false;
                    return;
                }
                else
                {
                    sliding = true;
                    return;
                }

            }


            if (sliding) 
            {
                Vector3 to_click = mouse_position - transform.position;

                Debug.Log("CONTACT " + col.contacts[0].point);
                Vector3 to_col = col.contacts[0].point - transform.position;

                Slide(to_col, to_click);
                return;
            }

        }
    }


    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Wall")
        {

            /*if (on_corner)
            {
                Debug.Log("Exited Corner");
                on_corner = false;
                fairy_rb.velocity = (Vector3.zero);
                mouse_position = transform.position;
            }
            else
            {*/
                mouse_position = mouse_position_holder;
                fairy_rb.velocity = GetVelocity(transform.position, mouse_position);
                on_wall = false;
                sliding = false;
            //}

        }
    }


    void Awake() {

        fairy_rb = GetComponent<Rigidbody>();
        fairy_rb.detectCollisions = true;
        mouse_position = transform.position;
        mouse_position_holder = mouse_position;
        on_wall = false;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButtonDown(0) && !on_wall)
        {
            RaycastHit ray_hit;
            if(!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out ray_hit, 100)) return;
            if (!ray_hit.transform) return;

            mouse_position_holder = mouse_position;
            mouse_position = new Vector3(ray_hit.point.x, transform.position.y, ray_hit.point.z);
           
            direction = GetVelocity(transform.position, mouse_position);

            Debug.DrawRay(transform.position, direction);
            fairy_rb.velocity = direction;
            return;
        }

        if ((new Vector3(transform.position.x, 0, transform.position.z) - new Vector3(mouse_position.x, 0, mouse_position.z)).magnitude < .1f)
        {
            fairy_rb.velocity = Vector3.zero;

        }


    }

}
