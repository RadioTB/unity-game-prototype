﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class MoveThief : MonoBehaviour
{
    float SPEED = 2.5f;

    Rigidbody thief_rb;
    Vector3 mouse_position;
    Vector3 mouse_position_holder;
    Vector3 direction;
    Vector3 inverse_normal;
    RaycastHit sphere_hit;
    Collider slide_collider;

    bool hit;
    float radius;
    int wall_mask;
    bool sliding;

    Vector3 GetVelocity(Vector3 v1, Vector3 v2)
    {
        return ((v2 - v1) / Vector3.Distance(v1, v2)).normalized * SPEED;
    }

    public void Stop()
    {
        thief_rb.velocity = Vector3.zero;
        mouse_position = transform.position;
        mouse_position_holder = transform.position;
    }

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("In Collision");
        thief_rb.velocity = Vector3.zero;
        mouse_position = Vector3.zero;
        mouse_position_holder = Vector3.zero;
    }

    bool OverGui()
    {
        bool over_gui = false;
        GameObject[] gui_objects = GameObject.FindGameObjectsWithTag("UI");
        Vector3[] corners = new Vector3[4];
        Rect bounds;
        for (int i = 0; i < gui_objects.Length; i++)
        {

            gui_objects[i].GetComponent<RectTransform>().GetWorldCorners(corners);
            bounds = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);

            //Debug.Log(bounds);
            //Debug.Log(Input.mousePosition);

            over_gui = bounds.Contains(Input.mousePosition);

        }
        //Debug.Log(over_gui);
        return over_gui;
    }

    // Use this for initialization
    void Start()
    {
        thief_rb = GetComponent<Rigidbody>();
        radius = GetComponent<SphereCollider>().radius;
        hit = false;
        mouse_position = transform.position;
        mouse_position_holder = transform.position;
        direction = Vector3.zero;
        wall_mask = 1 << 8;
        sliding = false;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        Vector3 radVector = new Vector3(pos.x - radius, pos.y, pos.z - radius);

        if (Input.GetMouseButtonDown(0) && !OverGui())
        {
            sliding = false;
            RaycastHit ray_hit;
            if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out ray_hit, 100)) return;
            Debug.Log(ray_hit.collider.GetType());
            mouse_position = new Vector3(ray_hit.point.x, pos.y, ray_hit.point.z);
            direction = GetVelocity(pos, mouse_position);

            Ray back = new Ray(pos, GetVelocity(mouse_position, pos));

            bool hit = Physics.SphereCast(radVector, radius, direction, out sphere_hit, Vector3.Distance(radVector, mouse_position) + radius, wall_mask);

            if (!hit)
            {
                Debug.DrawRay(pos, mouse_position - pos, Color.yellow, 1);
                mouse_position_holder = mouse_position;
                thief_rb.velocity = direction;
            }
            else
            {
                float hit_dist = sphere_hit.distance - radius;
                if (hit_dist > radius)
                {
                    direction = GetVelocity(pos, mouse_position);

                    mouse_position_holder = mouse_position;
                    mouse_position = (direction.normalized * hit_dist) + pos;

                    Debug.DrawRay(pos, sphere_hit.point - pos, Color.magenta, 1);
                    Debug.DrawRay(pos, mouse_position - pos, Color.blue, 1);
                    thief_rb.velocity = direction;
                }
                else
                {
                    mouse_position_holder = pos;
                    mouse_position = pos;
                }

            }
        }//end 1

        if (sliding)
        {
            if (!Physics.SphereCast(radVector, radius, inverse_normal, out sphere_hit, radius, wall_mask))
            {
                if (Physics.SphereCast(radVector, radius, mouse_position_holder - pos, out sphere_hit, Vector3.Distance(radVector, mouse_position_holder) + radius, wall_mask))
                {
                    Debug.DrawRay(pos, mouse_position_holder - pos, Color.magenta, 1);
                    direction = GetVelocity(pos, mouse_position_holder);

                    mouse_position = (direction.normalized * sphere_hit.distance) + pos;
                    thief_rb.velocity = direction;

                    sliding = false;
                }
                else
                {
                    Debug.DrawRay(pos, mouse_position_holder - pos, Color.magenta, 1);
                    mouse_position = mouse_position_holder;
                    direction = GetVelocity(pos, mouse_position_holder);
                    thief_rb.velocity = direction;
                    sliding = false;
                }
            }


            if (Vector3.Distance(pos, mouse_position) < .1f)
            {
                Debug.DrawRay(pos, mouse_position_holder - pos, Color.magenta, 1);
                mouse_position_holder = pos;
                sliding = false;
            }
        }//end 2

        if (Vector3.Distance(pos, mouse_position) < .1f)
        {
            if (Vector3.Distance(pos, mouse_position_holder) < .1f)
            {
                thief_rb.velocity = Vector3.zero;
                sliding = false;
            }
            else
            {

                RaycastHit find_norm;
                Physics.Raycast(pos, sphere_hit.point - pos, out find_norm);
                direction = Vector3.Cross(Vector3.Cross(find_norm.normal, mouse_position_holder - pos), find_norm.normal);
                inverse_normal = new Vector3(-find_norm.normal.x, find_norm.normal.y, -find_norm.normal.z);
                float angle = Vector3.Angle(inverse_normal, mouse_position_holder - pos);
                float mag = Mathf.Sin(angle * Mathf.Deg2Rad) * (mouse_position_holder - pos).magnitude;
                mouse_position = pos + (direction.normalized * mag);

                Debug.DrawRay(Vector3.zero, mouse_position, Color.green, 1);
                Debug.DrawRay(pos, mouse_position - pos, Color.cyan, 1);


                Debug.DrawRay(find_norm.point, find_norm.normal, Color.red, 1);



                if (Physics.SphereCast(radVector, radius, direction, out sphere_hit, Vector3.Distance(radVector, mouse_position), wall_mask))
                {
                    float hit_dist = sphere_hit.distance - radius;
                    if (hit_dist> radius)
                    {
                        direction = GetVelocity(pos, mouse_position);
                        mouse_position = pos + direction.normalized * hit_dist;
                        //mouse_position_holder = mouse_position;
                        thief_rb.velocity = direction;
                    }
                    else
                    {
                        mouse_position = pos;
                        mouse_position_holder = mouse_position;
                    }
                }
                else
                {
                    direction = GetVelocity(pos, mouse_position);
                    thief_rb.velocity = direction;
                }

                sliding = true;
            }

        }//end 3

    }
}
