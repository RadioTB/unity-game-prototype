﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class MoveThief2 : MonoBehaviour
{
    float SPEED = 1.5f;

    Rigidbody thief;
    Vector3 target_position;
    Vector3 inverse_normal;
    RaycastHit sphere_hit;
    Collider slide_collider;


    bool holding;
    float radius;
    float time_on_wall;
    int wall_mask;
    bool is_moving;
    int wall_counter;

    const int LEFT_MOUSE_BUTTON = 0;

    private void Start()
    {
        thief = transform.GetComponent<Rigidbody>();
        radius = GetComponent<SphereCollider>().radius;
        target_position = transform.position;

        wall_mask = 1 << 8;
        
        is_moving = false;

        wall_counter = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.collider.tag)
        {
            case "Wall":
                wall_counter += 1;
                break;

            default:
                break;
        }

    }

    private void OnCollisionStay(Collision collision)
    {
        switch (collision.collider.tag)
        {
            case "Wall":
                ShouldStop();
                break;

            default:
                break;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        switch (collision.collider.tag)
        {
            case "Wall":
                wall_counter -= 1;
                break;

            default:
                break;
        }
    }

    private void FixedUpdate()
    {
        thief.velocity = Vector3.zero;

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
        {
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float point = 0f;
            if (Physics.Raycast(ray, float.PositiveInfinity, GameObject.FindGameObjectWithTag("Floor").layer))
            {                
                target_position = ray.GetPoint(point);
            }
            else return;

            //SetTargetPosition(target_position);
            is_moving = true;
        }

        if (is_moving)
        {
            MovePlayer();
        }

        Debug.Log(is_moving);
    }

    void MovePlayer()
    {

        //transform.LookAt(target_position);
        thief.MovePosition(Vector3.MoveTowards(thief.position, target_position, SPEED * Time.deltaTime));


        Debug.Log(thief.velocity);

        if (transform.position == target_position)
        {
            is_moving = false;
        }

    }

    void ShouldStop()
    {
        RaycastHit ray_hit;


        if (Physics.Raycast(thief.position, target_position - thief.position, out ray_hit, float.PositiveInfinity, wall_mask))
        {
            // Debug.Log("Sphere Hit Point: " + ray_hit.point.ToString() + " Sphere Distance: " + ray_hit.distance);
            // Physics.Raycast(transform.position, target_position - transform.position, out ray_hit, float.PositiveInfinity, wall_mask);

            if (wall_counter == 2)
            {
                is_moving = false;
                target_position = thief.position;
            }

            Debug.Log("Angle " + Vector3.Angle(thief.position - target_position, ray_hit.normal));

            Debug.DrawLine(thief.position, ray_hit.point, Color.green, 2f);
            Debug.DrawLine(thief.position, target_position, Color.magenta, 2f);

            if (Vector3.Angle(thief.position - target_position, ray_hit.normal) < 1f)
            {
                is_moving = false;
                target_position = thief.position;
            }

            // Debug.DrawRay(transform.position, target_position - transform.position, Color.white);
            // Debug.Log("Ray Hit Point: " + ray_hit.point.ToString() + " Ray Distance: " + ray_hit.distance);

        }

    }


    /* Really cool movement, using spherecasting
     * currently using easier implementation with collieders/physics
     * want to save the code below
 
    void SetTargetPosition(Vector3 target)
    {
        RaycastHit ray_hit;


        Vector3.Angle(Vector3.forward, target);

        Vector3 negative_radius = (new Ray(transform.position, transform.position - target)).GetPoint(radius);

        Debug.DrawRay(transform.position, negative_radius - transform.position, Color.magenta, 5f);

        if (Physics.SphereCast(negative_radius, radius, target - transform.position, out ray_hit, Vector3.Distance(transform.position, target), wall_mask))
        {
            // Debug.Log("Sphere Hit Point: " + ray_hit.point.ToString() + " Sphere Distance: " + ray_hit.distance);
            // Physics.Raycast(transform.position, target_position - transform.position, out ray_hit, float.PositiveInfinity, wall_mask);

            Ray ray = new Ray(transform.position, target - transform.position);
            // Debug.DrawRay(transform.position, target_position - transform.position, Color.white);
            // Debug.Log("Ray Hit Point: " + ray_hit.point.ToString() + " Ray Distance: " + ray_hit.distance);

            target_position = ray.GetPoint(ray_hit.distance - radius);
        }

        is_moving = true;
    }

    */

}
