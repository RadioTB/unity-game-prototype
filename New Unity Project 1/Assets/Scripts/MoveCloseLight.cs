﻿using UnityEngine;
using System.Collections;

public class MoveCloseLight : MonoBehaviour {
    GameObject player;
    Light player_light;
    GameObject fairy_light;
    Vector3 player_position;
    float plight_distance;
    Ray player_to_fairy;
    Collider[] wall_colliders;
    float PLIGHTZ;
    

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        fairy_light = GameObject.FindGameObjectsWithTag("FairyLight")[0];
        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");

        wall_colliders = new Collider[walls.Length];

        for(int i = 0; i <walls.Length; i++)
        {
            wall_colliders[i] = walls[i].GetComponent<Collider>();
        }

        player_light = GetComponent<Light>();

        PLIGHTZ = transform.position.z;

        player_position = (Vector2)player.transform.position;

        plight_distance = Vector2.Distance((Vector2)player_light.transform.position, player_position);

        player_to_fairy = new Ray(player_position, fairy_light.transform.position - player_position);

        player_to_fairy.GetPoint(plight_distance);


    }
	
	// Update is called once per frame
	void Update () {

        Debug.DrawRay(player_position, fairy_light.transform.position-player_position, Color.red, 1, false);

        player_to_fairy = new Ray((Vector2)player_position, (Vector2)fairy_light.transform.position - (Vector2)player_position);

        RaycastHit wall_hit = new RaycastHit();
        bool rch = false;

        for (int i = 0; i < wall_colliders.Length; i++)
        {
           rch = wall_colliders[i].Raycast(player_to_fairy, out wall_hit, Vector2.Distance(player_position, fairy_light.transform.position));
            if (rch) break;
        }
        if (rch) player_light.enabled = false;
        else player_light.enabled = true;
        Vector3 light_position = player_to_fairy.GetPoint(plight_distance);
        light_position.z = PLIGHTZ;
        player_light.transform.position = light_position;
        
	}
}
