﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]

public class MoveFairy : MonoBehaviour {

    float SPEED = .01f;

    CharacterController fairy_cc;

    Vector3 mouse_position;
    Vector3 mouse_position_holder;

    bool stopped;
    bool set;
    bool on_wall;
    bool on_corner;
    float bh_angle;

    Vector3 GetVelocity(Vector3 v1, Vector3 v2)
    {
        return ((v2 - v1) / Vector3.Distance(v1, v2)).normalized * SPEED;
    }




    void FixedUpdate()
    {

    }
    /*
    void OnCollisionEnter(Collision col)
    {
        
        if(col.gameObject.tag == "Wall")
        {
            mouse_position_holder = mouse_position;
            
            fairy_cc.SimpleMove( Vector2.zero);
            set = false;
            stopped = true;

            Debug.Log("Entered Collision");
            if (on_wall)
            {
                on_corner = true;
                fairy_cc.SimpleMove(-1 * col.contacts[0].point);
            }
            else on_wall = true;
            Debug.Log("On Corner " + on_corner);
            
        }

    }

    void OnCollisionStay(Collision col)
    {

        if (col.gameObject.tag == "Wall")
        { 

            if (Input.GetMouseButton(0) && !set)
            {
                mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Debug.DrawRay(transform.position, mouse_position - (Vector2)transform.position, Color.yellow, 1);

                Debug.Log("CONTACT POINTS " + col.contacts.Length);
                Debug.Log("CONTACT POINT " + col.contacts[0].point);

                Vector2 to_click = mouse_position - (Vector2)transform.position;
                Vector2 to_col = (Vector2)col.contacts[0].point - (Vector2)transform.position;


                float angle = Vector2.Angle(to_col, to_click);



                if (angle > 89)
                {
                    fairy_cc.SimpleMove(GetVelocity(transform.position, mouse_position));
                    set = true;
                    return;
                }
                else
                {
                    set = false;
                    return;
                }
                
            }


            if (!set)
            {
                Vector2 to_click = mouse_position - (Vector2)transform.position;
                Debug.Log("CONTACT " + col.contacts[0].point);
                Debug.Log("CONTACT POINTS " + col.contacts.Length);
                Debug.Log("CENTER " + GetComponent<Collider>().bounds.center);
                Debug.Log("SET " + set);
                Vector2 to_col = (Vector2)col.contacts[0].point - (Vector2)transform.position;
                Slide(to_col, to_click);
                return;
            }

        }

    }


    void OnCollisionExit(Collision col)
    {
        if(col.gameObject.tag == "Wall")
        {
            
            if(on_corner)
            {
                Debug.Log("Exited Corner");
                on_corner = false;
                fairy_cc.SimpleMove(Vector2.zero);
                mouse_position = transform.position;
            }
            else
            {
                mouse_position = mouse_position_holder;
                fairy_cc.SimpleMove(GetVelocity(transform.position, mouse_position));
                on_wall = false;
                stopped = false;
                set = false;
            }

        }
        
    }
    */
    // Use this for initialization

    void Start () {

        fairy_cc = this.gameObject.GetComponent<CharacterController>();
        on_wall = false;
        on_corner = false;
        set = false;
        bh_angle = 1.5f;

    }


    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButton(0))
        {
            mouse_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            mouse_position = new Vector3(mouse_position.x, mouse_position.y, transform.position.z);
            fairy_cc.Move(GetVelocity(transform.position, mouse_position));
            Debug.DrawRay(transform.position, mouse_position - transform.position, Color.yellow, 1);
            set = true;
        }
         

    }

}
