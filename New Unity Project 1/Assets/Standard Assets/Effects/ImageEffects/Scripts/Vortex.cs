using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [ExecuteInEditMode]
    [AddComponentMenu("Image Effects/Displacement/Vortex")]
    public class Vortex : ImageEffectBase
    {
        public Vector2 radius = new Vector2(0.1F,0.1F);
        public float angle = 720;
        public Vector2 center = new Vector2(0.5F, 0.5F);

        private Camera cam;
        private Camera bh_cam;
        private GameObject bh;
        private Vector2 position;

        new void Start()
        {
            base.Start();
            bh_cam = GameObject.FindGameObjectWithTag("BH_Camera").GetComponent<Camera>();
            cam = Camera.main;
            bh = GameObject.FindGameObjectWithTag("BlackHole");
        }

        // Called by camera to apply image effect
        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            ImageEffects.RenderDistortion (material, source, destination, angle, center, radius);
        }
    }
}
